#include "leptonSDKEmb32PUB/LEPTON_SDK.h"
#include "leptonSDKEmb32PUB/LEPTON_SYS.h"
#include "leptonSDKEmb32PUB/LEPTON_Types.h"

#ifndef LEPTON_I2C
#define LEPTON_I2C

int lepton_connect();
void lepton_perform_ffc();
LEP_SYS_FPA_TEMPERATURE_KELVIN_T lepton_get_fpa_temperature();

#endif
