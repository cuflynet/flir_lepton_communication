#include "flir_lepton_communication/Lepton_I2C.h"

bool _connected;

LEP_CAMERA_PORT_DESC_T _port;

int lepton_connect() {
    LEP_OpenPort(1, LEP_CCI_TWI, 400, &_port);
    _connected = true;
    return 0;
}

void lepton_perform_ffc() {
    if(!_connected) {
        lepton_connect();
    }
    LEP_RunSysFFCNormalization(&_port);
}

LEP_SYS_FPA_TEMPERATURE_KELVIN_T lepton_get_fpa_temperature() {
    LEP_SYS_FPA_TEMPERATURE_KELVIN_T ret;

    if(!_connected){
        lepton_connect();
    }

    LEP_GetSysFpaTemperatureKelvin(&_port, &ret);
    return ret;
}

//presumably more commands could go here if desired
